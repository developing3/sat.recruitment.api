﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Sat.Recruitment.Services.Abstraction;
using Sat.Recruitment.Models.DTO;

namespace Sat.Recruitment.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public partial class UsersController : ControllerBase
    {
        private readonly IUserService _userService;
        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost]
        public async Task<ResultDTO> CreateUser(UserFormData userCreationDTO)
        {
            string errors = _userService.ValidateErrors(userCreationDTO);

            if (!string.IsNullOrWhiteSpace(errors))
                return new ResultDTO()
                {
                    IsSuccess = false,
                    Message = errors
                };
            try
            {
                var isSuccesfull = await _userService.UpdateAsync(userCreationDTO);
                return new ResultDTO()
                {
                    IsSuccess = isSuccesfull,
                    Message = isSuccesfull ? "User Created." : "The user is duplicated."
                };
            }
            catch (Exception ex)
            {
                return new ResultDTO()
                {
                    IsSuccess = false,
                    Message = "Hubo un error al realizar la operacion: " + ex.Message
                };
            }
        }
    }
}
