﻿namespace Sat.Recruitment.Models.DTO
{
    public class UserDTO
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string UserType { get; set; }
        public decimal? Money { get; set; }

        public UserDTO(string name, string email, string address, string phone, string userType = null, decimal? money = null)
        {
            Name = name;
            Email = email;
            Address = address;
            Phone = phone;
            UserType = userType;
            Money = money;
        }
        public override string ToString()
        {
            return Name + "," + Email + "," + Address + "," + Phone + "," + UserType + "," + Money;
        }
    }
}