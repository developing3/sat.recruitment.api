﻿namespace Sat.Recruitment.Models.DTO
{
    public class UserFormData
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string UserType { get; set; }
        public string Money { get; set; }

        public UserFormData(string Name, string Email, string Address, string Phone, string UserType, string Money)
        {
            this.Name = Name;
            this.Email = Email;
            this.Address = Address;
            this.Phone = Phone;
            this.UserType = UserType;
            this.Money = Money;
        }
    } 
}
