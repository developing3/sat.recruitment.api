﻿using System;

namespace Sat.Recruitment.Models
{
    public enum UserTypeEnum
    {
        Normal,
        SuperUser,
        Premium
    }
}