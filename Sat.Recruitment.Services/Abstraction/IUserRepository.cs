﻿using Sat.Recruitment.Models.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sat.Recruitment.Services.Abstraction
{
    public interface IUserRepository : IGenericRepository<UserDTO>
    {
        public new Task<List<UserDTO>> GetAsync();
        public new Task SaveAsync(UserDTO type);
    }
}