﻿using Sat.Recruitment.Models.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sat.Recruitment.Services.Abstraction
{
    public interface IUserService : IGenericService<UserDTO>
    {
        public Task<bool> UpdateAsync(UserFormData userCreationDTO);
        public new Task<bool> ExistAsync(UserDTO newUser);
        public string ValidateErrors(UserFormData user);
        public UserDTO CompleteDataUser(UserFormData userCreationDTO);
        public new Task<List<UserDTO>> GetAllUsersAsync();
    }
}

