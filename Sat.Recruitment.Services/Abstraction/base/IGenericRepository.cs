﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sat.Recruitment.Services.Abstraction
{
    public interface IGenericRepository<T> where T : class
    {
        public Task<List<T>> GetAsync();
        public Task SaveAsync(T type);
    }
}
