﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sat.Recruitment.Services.Abstraction
{
    public interface IGenericService<T> where T : class
    {
        public Task<bool> UpdateAsync(T entity);
        public Task<bool> ExistAsync(T entity);
        public string ValidateErrors(T entity);
        public Task<T> CompleteDataUser(T entity);
        public Task<List<T>> GetAllUsersAsync();
    }
}


