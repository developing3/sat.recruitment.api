﻿using Sat.Recruitment.Models;
using Sat.Recruitment.Models.DTO;
using System;

namespace Sat.Recruitment.Api.Helpers
{
    public class UserHelper
    {
        public decimal GetTotalByTypeUser(UserTypeEnum typeUser, decimal amount)
        {
            var tax = amount * GetPercentByTypeUser(typeUser, amount);
            return amount + tax;
        }
        public decimal GetPercentByTypeUser(UserTypeEnum typeUser, decimal amount)
        {
            decimal percent = 0;
            switch (typeUser)
            {
                case UserTypeEnum.Normal:
                    percent = amount > 100 ? Convert.ToDecimal(0.12) : amount > 10 ? Convert.ToDecimal(0.8) : 0;
                    break;
                case UserTypeEnum.SuperUser:
                    percent = Convert.ToDecimal(0.8);
                    break;
                case UserTypeEnum.Premium:
                    percent = Convert.ToDecimal(2);
                    break;
            }
            return percent;
        }
        public string NormalizeEmailUser(string email)
        {
            var aux = email.Split(new char[] { '@' }, StringSplitOptions.RemoveEmptyEntries);

            var atIndex = aux[0].IndexOf("+", StringComparison.Ordinal);

            aux[0] = atIndex < 0 ? aux[0].Replace(".", "") : aux[0].Replace(".", "").Remove(atIndex);

            email = string.Join("@", new string[] { aux[0], aux[1] });

            return email;
        }        
    }
}
