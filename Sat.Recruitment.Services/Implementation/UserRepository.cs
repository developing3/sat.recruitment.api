﻿using Sat.Recruitment.Models.DTO;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Sat.Recruitment.Services.Abstraction;
using Microsoft.Extensions.Configuration;
//using Castle.Core.Configuration;

namespace Sat.Recruitment.Services.Implementation
{
    public class UserRepository : IGenericRepository<UserDTO>, IUserRepository
    {
        private readonly IConfiguration configuration;
        public UserRepository(IConfiguration configuration)
        {
            this.configuration = configuration;
        }
        public async Task<List<UserDTO>> GetAsync()
        {
            return await GetFromFile();
        }
        public async Task SaveAsync(UserDTO user)
        {
            await WriteInFileAsync(user);
        }
        private async Task<List<UserDTO>> GetFromFile()
        {
            var usersFile = GetFile();
            var fileToObjectUsers = await FileToObject(usersFile);
            return fileToObjectUsers;
        }
        private StreamReader GetFile()
        {
            var stream = GetFileStream();
            var reader = new StreamReader(stream);
            return reader;
        }
        private FileStream GetFileStream()
        {
            string path = "./" + Directory.GetCurrentDirectory() + "PathUserFile";//configuration<string>("PathUserFile");
            return new FileStream(path, FileMode.Open, FileAccess.ReadWrite);
        }
        private async Task WriteInFileAsync(UserDTO user)
        {
            string path = "./" + Directory.GetCurrentDirectory() + "PathUserFile";//configuration.GetValue<string>("PathUserFile");
            await File.WriteAllTextAsync(path, user.ToString());
        }
        private async Task<List<UserDTO>> FileToObject(StreamReader reader)
        {
            var usersList = new List<UserDTO>();
            while (reader.Peek() >= 0)
            {
                var line = await reader.ReadLineAsync();
                string[] lineSplited = line.Split(',');
                var userFinal = new UserDTO
                (
                   lineSplited[0].ToString(),
                   lineSplited[1].ToString(),
                   lineSplited[2].ToString(),
                   lineSplited[3].ToString()
                );
                usersList.Add(userFinal);
            }
            reader.Close();
            return usersList;
        }
    }
}
