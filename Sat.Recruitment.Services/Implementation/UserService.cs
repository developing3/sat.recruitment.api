﻿using Sat.Recruitment.Api.Helpers;
using Sat.Recruitment.Models;
using Sat.Recruitment.Models.DTO;
using Sat.Recruitment.Models.Helpers;
using Sat.Recruitment.Services.Abstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sat.Recruitment.Services.Implementation
{
    public class UserService : IGenericService<UserDTO>, IUserService
    {
        private readonly IUserRepository UserRepository;
        private readonly UserHelper UserHelper;

        public UserService(IUserRepository UserRepository, UserHelper UserHelper)
        {
            this.UserRepository = UserRepository;
            this.UserHelper = UserHelper;
        }
        public async Task<bool> UpdateAsync(UserFormData userCreationDTO)
        {
            var user = CompleteDataUser(userCreationDTO);
            var exitsUser = await ExistAsync(user);

            if (!exitsUser)
            {
                await UserRepository.SaveAsync(user);
                return true;
            }
            else
                return false;
        }
        public async Task<bool> ExistAsync(UserDTO newUser)
        {
            var users = await UserRepository.GetAsync();

            Func<UserDTO, bool> condition = user => user.Email.Contains(newUser.Email) ||
                                                    user.Phone.Contains(newUser.Phone) ||
                                                    (user.Name.Contains(newUser.Name) && user.Address.Contains(newUser.Address));

            return users.Any(condition);
        }
        public string ValidateErrors(UserFormData user)
        {
            string errorMessage = string.Empty;
            if (string.IsNullOrWhiteSpace(user.Name))
                errorMessage += "The name is required \n";
            if (string.IsNullOrWhiteSpace(user.Email))
                errorMessage += "The email is required \n";
            if (string.IsNullOrWhiteSpace(user.Address))
                errorMessage += "The address is required \n";
            if (string.IsNullOrWhiteSpace(user.Phone))
                errorMessage += "The phone is required \n";

            if (string.IsNullOrWhiteSpace(user.UserType) || string.IsNullOrWhiteSpace(user.Money))
                throw new FormatException();

            return errorMessage;
        }
        public UserDTO CompleteDataUser(UserFormData userCreationDTO)
        {
            var typeUser = EnumHelper.ParseEnum<UserTypeEnum>(userCreationDTO.UserType);
            var completeUser = new UserDTO(userCreationDTO.Name, userCreationDTO.Email, userCreationDTO.Address, userCreationDTO.Phone, userCreationDTO.UserType, decimal.Parse(userCreationDTO.Money));
            completeUser.Money = UserHelper.GetTotalByTypeUser(typeUser, completeUser.Money.Value);
            completeUser.Email = UserHelper.NormalizeEmailUser(completeUser.Email);
            return completeUser;
        }
        public async Task<List<UserDTO>> GetAllUsersAsync()
        {
            return await UserRepository.GetAsync();
        }

        public Task<bool> UpdateAsync(UserDTO entity)
        {
            throw new NotImplementedException();
        }

        public string ValidateErrors(UserDTO entity)
        {
            throw new NotImplementedException();
        }

        public UserDTO CompleteDataUser(UserDTO entity)
        {
            throw new NotImplementedException();
        }

        Task<UserDTO> IGenericService<UserDTO>.CompleteDataUser(UserDTO entity)
        {
            throw new NotImplementedException();
        }
    }
}



