using Microsoft.Extensions.Configuration;
using Moq;
using Sat.Recruitment.Api.Controllers;
using Sat.Recruitment.Api.Helpers;
using Sat.Recruitment.Services.Implementation;
using Sat.Recruitment.Services.Abstraction;
using System;
using System.IO;
using Xunit;
using Sat.Recruitment.Models.DTO;

namespace Sat.Recruitment.Test
{
    public class UsersTest
    {
        private IUserRepository _userRepository;
        private IUserService _userService;
        private IConfiguration _config;

        private Mock<UserHelper> _userHelper;
        private Mock<UsersController> _usersController;

        public UsersTest()
        {
            _config = (new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build());
            _userHelper = new Mock<UserHelper>();
            _userRepository = new UserRepository(_config);
            _userService = new UserService(_userRepository, _userHelper.Object);
            _usersController = new Mock<UsersController>(_userService);
        }

        [Fact]
        public void SaveOk()
        {
            //Arrange
            _usersController = new Mock<UsersController>(_userService);

            //Act
            ResultDTO result = _usersController.Object.CreateUser(new UserFormData("Mike", "mike@gmail.com", "Av. Juan G", "+349 1122354215", "Normal", "124")).Result;

            //Assert
            Assert.True(result.IsSuccess);
        }

        [Fact]
        public void SaveErrorUserRepeat()
        {
            //Arrange
            _usersController = new Mock<UsersController>(_userService);

            //Act
            ResultDTO result = _usersController.Object.CreateUser(new UserFormData("Agustina", "Agustina@gmail.com", "Av. Juan G", "+349 1122354215", "Normal", "124")).Result;

            //Assert
            Assert.False(result.IsSuccess);
        }

        [Theory]
        [InlineData("Agustina", "Agustina@gmail.com", "Av. Juan G", "+349 1122354215", "Normal", "124")]
        public void SaveErrorWithParametersFromInlineData(string name, string email, string address, string phone, string userType, string money)
        {
            //Arrange
            _usersController = new Mock<UsersController>(_userService);

            //Act
            ResultDTO result = _usersController.Object.CreateUser(new UserFormData(name, email, address, phone, userType, money)).Result;

            //Assert
            Assert.False(result.IsSuccess);
        }

        [Fact]
        public void SaveErrorMissingParametersExcepcion()
        {
            //Arrange
            _usersController = new Mock<UsersController>(_userService);

            //Act

            //Assert
            Assert.Throws<AggregateException>(() => _usersController.Object.CreateUser(new UserFormData("Agustina", "Agustina@gmail.com", "Av. Juan G", "+349 1122354215", null, null)).Result);
        }
    }
}
